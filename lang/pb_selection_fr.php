<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//A
	'ajouter_article_numero' => 'Ajouter',

	// P
	'selection_articles' => 'Sélection d’articles',
	'selection_article_numero' => 'Sélectionner l’article numéro : ',
	
	//R
	'retirer_de_la_liste' => 'Retirer de la liste',
);
