<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pb_selection_description' => 'Le plugin Sélection d\'articles permet de
		 créer des sélections d\'articles dans les rubriques. 
		 Il ne doit être utilisé que ponctuellement, pour 
		 réaliser par exemple des pages de Une (là où, 
		 auparavant, on utilisait des mots-clés du type Une1, Une2...).',
	'pb_selection_nom' => 'Sélection d\'articles',
	'pb_selection_slogan' => 'Sélectionner des articles à mettre en avant',
);
